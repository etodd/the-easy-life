re.c('anim')
.requires('tween')
.namespaces(
{
	frame: 0,
	animation:false,
	advanceFrame: function()
	{
		if (this.anim_animation)
		{
			if (this.anim_frame < this.anim_animation.length)
			{
				var frame = this.anim_animation[this.anim_frame];
				this.tween(frame.time, frame.attrs);
				this.anim_frame++;
			}
			else
				this.trigger('anim:finish', this.anim_animation);
		}
	}
})
.defines(
{
	play: function(animation)
	{
		this.attr(animation[0].attrs);
		this.anim_frame = 1;
		this.anim_animation = animation;
		this.anim_advanceFrame();
		return this;
	}
})
.init(function()
{
	this.on('tween:finish', this.anim_advanceFrame);
})
.dispose(function()
{
	
});
