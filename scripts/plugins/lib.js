(function(re)
{
	re.loadScene = function(scene, callback)
	{
		$.get('assets/images/' + scene + '.svg', callback);
	};
	
	re.getSVGElementPosition = function(svgData, id)
	{
		return re._getPositionFromSVGElement(svgData, svgData.getElementById(id));
	};
	
	re._getPositionFromSVGElement = function(svgData, e)
	{
		var svgRoot = svgData.getElementsByTagName('svg')[0];
		
		var xform;
		if (!(xform = e.getCTM()))
		{
			// Firefox
			xform = e.transform.animVal[0];
			if (xform)
				xform = xform.matrix;
			else // For identity transforms, Firefox decides its a good idea to have NO transform at all.
				xform = svgRoot.createSVGMatrix(); // Identity matrix
		}
		
		var scaleX = Math.sqrt(xform.a * xform.a + xform.b * xform.b) * (xform.a < 0 ? -1 : 1);
		var scaleY = Math.sqrt(xform.c * xform.c + xform.d * xform.d) * (xform.d < 0 ? -1 : 1);
		var rotation = Math.atan2(xform.b, xform.a * (xform.a < 0 ? -1 : 1)) * (xform.a < 0 ? -1 : 1) * 180.0 / (2.0 * Math.PI);
		
		if (scaleX < 0 && scaleY < 0)
		{
			scaleX *= -1;
			scaleY *= -1;
			rotation += 180.0;
		}
		
		var pt = svgRoot.createSVGPoint();
		pt.x = e.x.animVal.value;
		pt.y = e.y.animVal.value;
		var globalPoint = pt.matrixTransform(xform);
		
		return {
			scaleX: scaleX,
			scaleY: scaleY,
			rotation: rotation,
			posX: globalPoint.x,
			posY: globalPoint.y,
		};
	};
	
	re.getSVGAnimation = function(svgData, c)
	{
		var elements = svgData.getElementsByClassName(c);
		var result = [];
		for (var i = 0; i < elements.length; i++)
		{
			var e = elements[i];
			result.push(
			{
				time: parseFloat(e.id.split(':')[1]),
				attrs: re._getPositionFromSVGElement(svgData, e),
			});
		}
		result.sort(function(a, b) { return a.time - b.time; });
		for (var i = result.length - 1; i > 0; i--)
		{
			result[i].time -= result[i - 1].time;
		}
		return result;
	};
})(re);