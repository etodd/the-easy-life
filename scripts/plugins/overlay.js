re.c('overlay')
.requires('draw')
.defines(
{
	opacity: 1,
	color: { r: 0, g: 0, b: 0 },
	draw: function(context)
	{
		context.fillStyle = 'rgba(' + this.color.r + ',' + this.color.g + ',' + this.color.b + ',' + this.opacity + ')';
		context.fillRect(0, 0, context.canvas.width, context.canvas.height);
	}
})
.init(function()
{
	
})
.dispose(function()
{
	
});
