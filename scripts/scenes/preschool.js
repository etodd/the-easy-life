re.scene('preschool')
.enter(function(svg)
{
	re.log('Entered preschool scene.');
	
	re('#fade')[0].tween(1.0, { opacity: 0 }); // Fade in
	
	re.e('image preschool.img align');
	
	var puzzleSolved = false;
	
	var kidHead = re.e('image kid-head.img mouse hit')
	.attr(
	{
		regX: 27,
		regY: 74,
	})
	.attr(re.getSVGElementPosition(svg, 'kid-head'))
	.on('click', function(x, y)
	{
		if (this.hit(x, y, 0, 0))
		{
			this.off('click');
			
			// Show thought bubble
			re.e('image thought-bubble.img tween')
			.attr(re.getSVGElementPosition(svg, 'thought-bubble'))
			.attr(
			{
				regX: 400,
				regY: 390,
				scaleX: 0,
				scaleY: 0,
			})
			.on('tween:finish', function()
			{
				// Build puzzle
				var order = [], positions = [], pieces = [];
				
				var basePos = re.getSVGElementPosition(svg, 'puzzle');
				for (var i = 0; i < 18; i++)
				{
					var x = i % 6, y = Math.floor(i / 6);
					order.push(i);
					positions.push(
					{
						scaleX: basePos.scaleX,
						scaleY: basePos.scaleY,
						rotation: basePos.rotation,
						posX: basePos.posX + (x * 100),
						posY: basePos.posY + (y * 100),
					});
				}
				
				order.sort(function() { return re.random() - 0.5 });
				
				for (var i = 0; i < 18; i++)
				{
					var piece = re.e('sprite puzzle.img mouse hit drag')
					.attr({ sizeX: 100, sizeY: 100 })
					.attr(positions[i])
					.attr({ basePosition: positions[i] })
					.on('mousedown', function(x, y)
					{
						if (!puzzleSolved)
						{
							this.drawLast();
							if (this.hit(x, y, 0, 0))
							{
								re.c('puzzle-pickup.sfx').sound.play();
								this.dragStart(x, y);
							}
						}
					})
					.on('mousemove', function(x, y)
					{
						if (this.dragging)
							this.dragUpdate(x, y);
					})
					.on('mouseup', function(x, y)
					{
						if (this.dragging)
						{
							re.c('puzzle-drop.sfx').sound.play();
							this.dragFinish();
							
							var closestDistance = 100, closestPosition = this.frame();
							for (var j = 0; j < positions.length; j++)
							{
								var pos = positions[j];
								var distance = re.distance(this.posX, this.posY, pos.posX, pos.posY);
								if (distance < closestDistance)
								{
									closestDistance = distance;
									closestPosition = j;
								}
							}
							
							this.attr(this.basePosition); // Move back to original position
							var otherFrame = pieces[closestPosition].frame();
							pieces[closestPosition].frame(this.frame());
							this.frame(otherFrame);
							
							var scrambled = false;
							for (var j = 0; j < pieces.length; j++)
							{
								if (pieces[j].frame() != j)
								{
									scrambled = true;
									break;
								}
							}
							
							if (!scrambled)
							{
								// Puzzle is solved
								puzzleSolved = true;
								
								re.c('puzzle-solve.sfx').sound.play();
								setTimeout(function()
								{
									re('#fade')[0].drawLast();
									re('#fade')[0].tween(1, {opacity: 1}); // Fade out
								}, 4000);
								
								setTimeout(function()
								{
									re.scene('load').enter('house'); // Done fading out, load the next scene
								}, 6000);
							}
						}
					});
					piece.frame(order[i]);
					pieces.push(piece);
				}
			})
			.tween(1.0, { scaleX: 1, scaleY: 1 });
		}
	});
	
	re('#fade')[0].drawLast();
})
.exit(function(nextScene)
{
	re('draw !overlay').dispose();
});