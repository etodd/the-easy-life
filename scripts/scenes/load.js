re.scene('load')
.enter(function(nextScene)
{
	if (nextScene)
	{
		re.log('Downloading "' + nextScene + '" scene SVG...');
		re.loadScene(nextScene, function(svg)
		{
			re.log('Download complete. Entering scene.');
			re.scene(nextScene).enter(svg);
		});
	}
	else
	{
		var musicExtension = re.support('ogg', 'mp3');
		re.assets =
		{
			sounds:
			[
				'sounds/sparkle-blue.' + musicExtension,
				'sounds/birds.' + musicExtension,
				'sounds/pop.wav',
				'sounds/leaves.wav',
				'sounds/puzzle-solve.wav',
				'sounds/click.wav',
				'sounds/drawer.wav',
				'sounds/puzzle-pickup.wav',
				'sounds/puzzle-drop.wav',
				'sounds/button.wav',
				'sounds/paper.wav',
				'sounds/punch.wav',
				'sounds/button-show.wav',
			],
			images:
			[
				'images/splash.svg',
				'images/girl.svg',
				'images/parents.svg',
				'images/kite.svg',
				'images/ball.svg',
				'images/fence.svg',
				'images/tree.svg',
				'images/dad-boy.svg',
				'images/heart.svg',
				'images/preschool.svg',
				'images/kid-head.svg',
				'images/thought-bubble.svg',
				'images/puzzle.svg',
				'images/house.svg',
				'images/window-rain.svg',
				'images/mom-couch.svg',
				'images/mom-head.svg',
				'images/thought-bubble2.svg',
				'images/heart-desaturated.svg',
				'images/minus.svg',
				'images/plus.svg',
				'images/girls.svg',
				'images/paper.svg',
				'images/flyer.svg',
				'images/monitor.svg',
				'images/mom-walk.svg',
				'images/kid-computer.svg',
				'images/robotics.svg',
				'images/key.svg',
				'images/kid-desk.svg',
				'images/desk.svg',
				'images/prof-walk.svg',
				'images/speech-bubble.svg',
				'images/bedroom.svg',
				'images/drawer.svg',
				'images/speech-bubble2.svg',
				'images/act-bubble.svg',
				'images/fin.svg',
				'images/kid-backpack.svg',
				'images/scholarship.svg',
				'images/speech-bubble3.svg',
				'images/button.svg',
				'images/button-housing.svg',
				'images/glove.svg',
			]
		};
		
		var background = re.e('rect')
		.attr(
		{
			color: '#000',
			posX: 0,
			posY: 0,
			sizeX: 800,
			sizeY: 600,
		});
		
		var loadingBackground = re.e('rect')
		.attr(
		{
			color: '#444',
			posX: 350,
			posY: 290,
			sizeX: 100,
			sizeY: 20,
		});
		
		var loadingBar = re.e('rect')
		.attr(
		{
			color: '#fff',
			posX: 350,
			posY: 290,
			sizeX: 0,
			sizeY: 20,
		});
		
		re.log('Loading...');
		re.load(re.assets)
		.progress(function(current, total, asset)
		{
			var percent = (current / total) * 100;
			re.log(percent + '% complete');
			loadingBar.attr({ sizeX: percent });
		})
		.complete(function()
		{
			re.log('Finished loading.');
			re.scene('home').enter();
		});
	}
})
.exit(function(nextScene)
{
	re('draw !overlay').dispose();
});