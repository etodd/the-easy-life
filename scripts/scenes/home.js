re.scene('home')
.enter(function()
{
	re.log('Entered home scene.');

	// Splash image
	re.e('image splash.img align');
	
	re.e('mouse').on('click', function()
	{
		re('#fade')[0].tween(0.5, {opacity: 1});
		
		setTimeout(function()
		{
			// Done fading out
			re.scene('load').enter('parents');
		}, 1000);
	});
	
	var fade = re.e('overlay tween')
	.attr({ id: 'fade', opacity: 1 })
	.on('tween:start', function()
	{
		this.drawable = true;
	})
	.on('tween:finish', function()
	{
		if (this.opacity == 0)
			this.drawable = false; // Done fading in, hide the overlay
	});
	
	re('#fade')[0].tween(0.5, { opacity: 0 }); // Fade in
})
.exit(function(nextScene)
{
	re('draw !overlay').dispose();
	re('mouse').dispose();
	
	var music = re.e('sound sparkle-blue.sfx')
	.attr({ id: 'music' })
	.on('sound:finish', function()
	{
		this.play();
	});
	music.play();
});