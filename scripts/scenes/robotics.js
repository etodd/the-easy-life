re.scene('robotics')
.enter(function(svg)
{
	re.log('Entered robotics scene.');
	
	re('#fade')[0].tween(1.0, { opacity: 0 }); // Fade in
	
	re.e('image robotics.img align');
	
	var kid = re.e('sprite kid-desk.img')
	.attr(re.getSVGElementPosition(svg, 'kid-desk'))
	.attr(
	{
		sizeX: 200,
		sizeY: 300,
	});
	
	var desk = re.e('image desk.img')
	.attr(re.getSVGElementPosition(svg, 'desk'));
	
	var numbers = re.e('text')
	.attr(
	{
		text: '',
		textColor: '#fff',
		font: '68px Courier New'
	})
	.attr(re.getSVGElementPosition(svg, 'numbers'));
	
	var targetNumber = 1317;
	
	var keyPos = re.getSVGElementPosition(svg, 'keys');
	
	var slope = Math.asin(keyPos.rotation * Math.PI / 180.0);
	
	var keyPositions = [], keys = [];
	
	var solved = false;
	
	for (var i = 0; i < 11; i++)
	{
		var pos = {
			regX: 16,
			regY: 16,
			sizeY: 32,
			sizeX: 32,
			posX: keyPos.posX + (i * 16),
			posY: keyPos.posY + (slope * i * 16),
			rotation: keyPos.rotation,
			scaleX: keyPos.scaleX,
			scaleY: keyPos.scaleY,
		};
		keyPositions.push(pos);
		
		keys.push(re.e('sprite key.img')
			.attr(pos)
			.frame(re.random() > 0.5 ? 1 : 0)
		);
	}
	
	var updateNumbers = function(firstTime)
	{
		var sum = 0;
		for (var i = 0; i < keys.length; i++)
			sum += keys[i].frame() * (1 << (keys.length - i - 1));
		
		if (sum == targetNumber)
		{
			if (firstTime)
			{
				// We randomly found the target number right off the bat. Invert the bits to get a different number.
				for (var i = 0; i < keys.length; i++)
					keys[i].frame(!keys[i].frame());
				
				for (var i = 0; i < keys.length; i++)
					sum += keys[i].frame() * (1 << (keys.length - i - 1));
			}
			else
			{
				// Solved the puzzle
				re.c('puzzle-solve.sfx').sound.play();
				solved = true;
				kid.frame(1);
				
				setTimeout(function()
				{
					// Prof walks in
					var prof = re.e('sprite prof-walk.img anim flicker')
					.attr(
					{
						sizeX: 200,
						sizeY: 600,
					})
					.play(re.getSVGAnimation(svg, 'prof-path'))
					.flicker(0.5, [0, 1], -1)
					.on('anim:finish', function()
					{
						kid.frame(2);
						this.flicker();
						this.frame(2);
						setTimeout(function()
						{
							var speechBubble = re.e('image speech-bubble.img tween')
							.attr(re.getSVGElementPosition(svg, 'speech-bubble'))
							.attr(
							{
								regX: 60,
								regY: 150,
								scaleX: 0,
								scaleY: 0,
							})
							.tween(0.5, { scaleX: 1, scaleY: 1 })
							.on('tween:finish', function()
							{
								setTimeout(function()
								{
									kid.frame(3); // Shocked face
								}, 1000);
								
								setTimeout(function()
								{
									re('#fade')[0].drawLast();
									re('#fade')[0].tween(1, {opacity: 1}); // Fade out
								}, 3000);
								
								setTimeout(function()
								{
									re.scene('load').enter('bedroom'); // Done fading out, load the next scene
								}, 5000);
							});
						}, 1000);
					});
				}, 1000);
			}
		}
		
		sum = sum.toString();
		while (sum.length < 4)
			sum = '0' + sum;
		numbers.attr({ text: sum });
	};
	
	updateNumbers(true);
	
	re.e('mouse').on('click', function(x, y)
	{
		if (!solved)
		{
			var closestKey = -1, closestDistance = 32;
			for (var i = 0; i < keyPositions.length; i++)
			{
				var pos = keyPositions[i];
				var distance = re.distance(pos.posX, pos.posY, x, y);
				if (distance < closestDistance)
				{
					closestKey = i;
					closestDistance = distance;
				}
			}
			
			if (closestKey != -1)
			{
				re.c('click.sfx').sound.play();
				var key = keys[closestKey];
				key.frame(!key.frame());
				updateNumbers();
			}
		}
	});
	
	re('#fade')[0].drawLast();
})
.exit(function(nextScene)
{
	re('draw !overlay').dispose();
});