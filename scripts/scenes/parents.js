re.scene('parents')
.enter(function(svg)
{
	re.log('Entered parents scene.');
	
	re('#fade')[0].tween(1.0, { opacity: 0 }); // Fade in
	
	var birds = re.e('sound birds.sfx')
	.attr({ id: 'birds' })
	.on('sound:finish', function()
	{
		this.play();
	});
	birds.play();
	
	re.e('image parents.img align');
	
	var kiteString = re.e()
	.attr(
	{
		draw: function(context)
		{
			context.lineWidth = 2;
			context.strokeStyle = '#000000';
			context.beginPath();
			context.moveTo(0, 0);
			context.lineTo(kite.posX - this.posX, kite.posY - this.posY);
			context.stroke();
			return this;
		}
	})
	.comp('draw');
	
	var kite = re.e('image kite.img drag mouse tween hit');
	
	var tree = re.e('image tree.img');
	
	var boyInTree = false;
	
	var girlLookingAtBoy = false;
	
	var girlAtBottomOfHill = false;
	
	var boy = re.e('sprite dad-boy.img flicker anim')
	.attr(
	{
		sizeX: 150,
		sizeY: 150,
		regX: 75,
		regY: 150,
	});
	
	var fence = re.e('image fence.img');
	
	var girl = re.e('sprite girl.img flicker anim')
	.attr(
	{
		sizeX: 100,
		sizeY: 150,
		regX: 50,
		regY: 150,
	});
	
	var girlLookAtBoy = function()
	{
		girlLookingAtBoy = true;
		girl.flicker();
		girl.frame(7);
		boy.flicker();
		boy.frame(4);
		
		setTimeout(function()
		{
			re.e('image heart.img tween')
			.attr(re.getSVGElementPosition(svg, 'heart'))
			.attr({ id: 'heart', regX: 32, regY: 32, scaleX: 0, scaleY: 0 })
			.tween(0.1, { scaleX: 1, scaleY: 1 });
			re.c('pop.sfx').sound.play();
		}, 3000);
		
		setTimeout(function()
		{
			re('#fade')[0].tween(1, {opacity: 1});
		}, 5000);
		
		setTimeout(function()
		{
			// Done fading out
			var heart = re('#heart')[0];
			heart.tween(0.25, { scaleX: 0, scaleY: 0 });
		}, 6000);
		
		setTimeout(function()
		{
			re.scene('load').enter('preschool'); // Done animating heart, load the next scene
		}, 6500);
	};
	
	girl.on('anim:finish', function()
	{
		girl.flicker(); // Stop running
		girl.frame(0);
		girlAtBottomOfHill = true;
		if (boyInTree)
			setTimeout(girlLookAtBoy, 2000);
	});
	
	var boyPath1 = re.getSVGAnimation(svg, 'boy-path1'), boyPath2 = re.getSVGAnimation(svg, 'boy-path2');
	
	boy.attr({ visible: false });
	
	boy.on('anim:finish', function(animation)
	{
		if (animation == boyPath1)
		{
			boy.flicker(0.25, [2, 3], -1);
			boy.play(boyPath2);
		}
		else
		{
			// Done climbing
			boyInTree = true;
			boy.flicker();
			if (girlAtBottomOfHill)
				setTimeout(girlLookAtBoy, 2000);
		}
	});
	
	var boyClimbTree = function()
	{
		boy.attr({ visible: true });
		boy.play(boyPath1);
		boy.flicker(0.25, [0, 1], -1);
	};
	
	var ball = re.e('image ball.img anim mouse hit');
	
	var ballPath, ballLostPath, girlPath;
	
	var lostBall = false;
	
	var kick = function()
	{
		if (!lostBall)
		{
			girl.flicker(0.5, [0, 1, 2, 1, 0]);
			
			setTimeout(function()
			{
				if (!lostBall)
					ball.play(ballPath)
			}, 250);
			
			setTimeout(kick, 2000);
		}
	};
	
	ball.on('click', function(x, y)
	{
		if (!lostBall && this.hit(x, y, 0, 0))
		{
			girl.flicker();
			lostBall = true;
			this.play(ballLostPath);
			
			setTimeout(function() { girl.play(girlPath); girl.flicker(0.4, [3, 4, 5, 6], -1); }, 1000);
		}
	});
	
	var kiteBasePosition;
	
	var kiteTarget;
	
	var kiteStuck = false;
	
	var stickKite = function()
	{
		kiteStuck = true;
		kite.attr(kiteTarget);
		setTimeout(boyClimbTree, 3000);
		re.c('leaves.sfx').sound.play();
	};
	
	kite.attr(
	{
		regX: 35,
		regY: 44,
	})
	.on('mousedown', function(x, y)
	{
		if (!kiteStuck && this.hit(x, y, 0, 0))
		{
			this.tween(); // Stop tween
			this.dragStart(x, y);
		}
	})
	.on('mousemove', function(x, y)
	{
		if (this.dragging && !kiteStuck)
		{
			var dx = x - kiteBasePosition.posX, dy = y - kiteBasePosition.posY;
			var distance = Math.sqrt(dx * dx + dy * dy);
			if (distance > 130)
			{
				var ratio = 130 / distance;
				dx *= ratio;
				dy *= ratio;
				x = kiteBasePosition.posX + dx;
				y = kiteBasePosition.posY + dy;
			}
			
			dx = x - kiteTarget.posX;
			dy = y - kiteTarget.posY;
			if (Math.sqrt(dx * dx + dy * dy) < 50)
				stickKite();
			else
				this.dragUpdate(x, y);
		}
	});
	
	var kiteSway = function()
	{
		if (!kiteStuck && !this.dragging)
		{
			kite.tween(1200,
			{
				posX: kiteBasePosition.posX + re.random(-50, 50),
				posY: kiteBasePosition.posY + re.random(-50, 50),
				rotation: re.random(-20, 20),
			});
		}
	};
	
	kite.on('mouseup', function(x, y)
	{
		if (this.dragging)
		{
			this.dragFinish();
			kiteSway();
		}
	});
	
	kite.on('tween:finish', kiteSway);
	
	fence.drawAfter(kiteString);
	
	girl.attr(re.getSVGElementPosition(svg, 'girl'));
		
	kite.attr(re.getSVGElementPosition(svg, 'kite'));
	
	kiteString.attr(re.getSVGElementPosition(svg, 'kiteString'));
	
	tree.attr(re.getSVGElementPosition(svg, 'tree'));
	
	fence.attr(re.getSVGElementPosition(svg, 'fence'));
	
	kiteBasePosition = { posX: kite.posX, posY: kite.posY };
	
	kiteTarget = re.getSVGElementPosition(svg, 'kiteTarget');
	
	ballPath = re.getSVGAnimation(svg, 'ball-path');
	
	ballLostPath = re.getSVGAnimation(svg, 'ball-lost-path');
	
	girlPath = re.getSVGAnimation(svg, 'girl-path');
	
	ball.attr(ballPath[0].attrs);
		
	kiteSway();
		
	kick();
	
	re('#fade')[0].drawLast();
})
.exit(function(nextScene)
{
	re('draw !overlay').dispose();
	re('#birds')[0].pause();
	re('#birds').dispose();
});