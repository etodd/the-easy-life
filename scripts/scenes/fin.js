re.scene('fin')
.enter(function(svg)
{
	re.log('Entered fin scene.');
	
	re('#fade')[0].tween(1.0, { opacity: 0 }); // Fade in
	
	var birds = re.e('sound birds.sfx')
	.attr({ id: 'birds' })
	.on('sound:finish', function()
	{
		this.play();
	});
	birds.play();
	
	re.e('image fin.img align');
	
	var kid = re.e('image kid-backpack.img tween')
	.attr(re.getSVGElementPosition(svg, 'kid'))
	.attr(
	{
		regX: 67,
		regY: 402,
	});
	
	var scholarship = re.e('image scholarship.img tween mouse')
	.attr(re.getSVGElementPosition(svg, 'scholarship'));
	setTimeout(function()
	{
		scholarship.on('click', function(x, y)
		{
			re.c('paper.sfx').sound.play();
			scholarship.tween(1, re.getSVGElementPosition(svg, 'scholarship-end'))
			.on('tween:finish', function()
			{
				this.dispose();
			});
			
			setTimeout(function()
			{
				var speechBubble = re.e('image speech-bubble3.img tween mouse')
				.attr(re.getSVGElementPosition(svg, 'speech-bubble'))
				.attr(
				{
					regX: 60,
					regY: 150,
					scaleX: 0,
					scaleY: 0,
				})
				.tween(0.5, { scaleX: 1, scaleY: 1 })
				.on('tween:finish', function()
				{
					this.on('click', function(x, y)
					{
						this.off('click');
						
						re.c('button-show.sfx').sound.play();
						
						var housing = re.e('image button-housing.img tween')
						.attr(re.getSVGElementPosition(svg, 'button-housing-start'))
						.tween(1, re.getSVGElementPosition(svg, 'button-housing-end'));
						
						var button = re.e('image button.img tween mouse hit')
						.attr(re.getSVGElementPosition(svg, 'button-start'))
						.tween(1, re.getSVGElementPosition(svg, 'button-end'));
						
						button.on('click', function(x, y)
						{
							if (this.hit(x, y, 0, 0))
							{
								this.off('click');
								re.c('puzzle-drop.sfx').sound.play();
								// Punch kid
								button.attr(re.getSVGElementPosition(svg, 'button-pressed'));
								
								var glove = re.e('image glove.img tween')
								.attr(re.getSVGElementPosition(svg, 'glove-start'))
								.tween(0.1, re.getSVGElementPosition(svg, 'glove-end'));
								
								setTimeout(function()
								{
									re('#music')[0].pause();
									re('#music').dispose();
									speechBubble.dispose();
									re.c('punch.sfx').sound.play();
									kid.tween(0.2, re.getSVGElementPosition(svg, 'kid-end'));
								}, 100);
								
								setTimeout(function()
								{
									re.c('button-show.sfx').sound.play();
									housing.tween(1, re.getSVGElementPosition(svg, 'button-housing-start'));
									button.tween(1, re.getSVGElementPosition(svg, 'button-start'));
								}, 2000);
								
								setTimeout(function()
								{
									kid.dispose();
									housing.dispose();
									button.dispose();
								}, 3000);
								
								setTimeout(function()
								{
									re.e('text align')
									.attr(
									{
										text: 'fin.',
										textColor: '#000',
										font: '48px sans-serif'
									})
									.alignHor()
									.alignVer();
								}, 5000);
							}
						});
					});
				});
			}, 2000);
		});
	}, 1500);
	
	re('#fade')[0].drawLast();
})
.exit(function(nextScene)
{
	re('draw !overlay').dispose();
	re('#birds')[0].pause();
	re('#birds').dispose();
});