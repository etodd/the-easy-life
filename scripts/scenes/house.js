re.scene('house')
.enter(function(svg)
{
	re.log('Entered house scene.');
	
	re('#fade')[0].tween(1.0, { opacity: 0 }); // Fade in
	
	re.e('image house.img align');
	
	re.e('sprite window-rain.img flicker')
	.attr({ sizeX: 250, sizeY: 250 })
	.attr(re.getSVGElementPosition(svg, 'window'))
	.flicker(0.3, [0, 1, 2], -1);
	
	var love = 0;
	
	var momSittingUp = false;
	
	var kidComputer, monitor;
	
	var paperPosition = re.getSVGElementPosition(svg, 'paper'), paperTarget = re.getSVGElementPosition(svg, 'paper-target');
	var paper = re.e('image paper.img mouse hit drag')
	.attr(paperPosition)
	.on('mousedown', function(x, y)
	{
		if (momSittingUp)
		{
			if (this.hit(x, y, 0, 0))
			{
				this.drawLast();
				this.dragStart(x, y);
			}
		}
	})
	.on('mousemove', function(x, y)
	{
		if (this.dragging)
			this.dragUpdate(x, y);
	})
	.on('mouseup', function(x, y)
	{
		if (this.dragging)
		{
			this.dragFinish();
			if (re.distance(this.posX, this.posY, paperTarget.posX, paperTarget.posY) < 70)
			{
				// Dropped the paper on mom
				this.dispose();
				
				re.c('paper.sfx').sound.play();
				
				// Show flyer
				var flyer = re.e('image flyer.img mouse hit tween')
				.attr({ regX: 232, regY: 255, posX: 400, posY: 300, scaleX: 0, scaleY: 0 })
				.tween(0.5, { scaleX: 1, scaleY: 1 });
				setTimeout(function()
				{
					flyer.on('click', function()
					{
						flyer.tween(0.5, { scaleX: 0, scaleY: 0 })
						.on('tween:finish', function()
						{
							this.dispose();
							
							// Mom gets up and drags kid to robotics
							momCouch.flicker(1, [3, 2]);
							setTimeout(function()
							{
								momCouch.flicker();
								momCouch.frame(4);
								var momPath1 = re.getSVGAnimation(svg, 'mom-path1'), momPath2 = re.getSVGAnimation(svg, 'mom-path2');
								var momWalk = re.e('sprite mom-walk.img anim flicker')
								.attr({ sizeX: 350, sizeY: 500 })
								.flicker(0.7, [0, 1], -1)
								.play(momPath1)
								.on('anim:finish', function(anim)
								{
									if (anim == momPath1)
									{
										// Pick up kid
										this.flicker(0.7, [2, 3], -1)
										this.play(momPath2);
										kidComputer.dispose();
									}
									else
									{
										// Mom done walking off-screen
										re('#fade')[0].drawLast();
										re('#fade')[0].tween(1, {opacity: 1}); // Fade out
										
										setTimeout(function()
										{
											re.scene('load').enter('robotics'); // Done fading out, load the next scene
										}, 2000);
									}
								});
								kidComputer.drawLast();
								monitor.drawLast();
							}, 1000);
						});
					});
				}, 1000);
			}
			else
				this.attr(paperPosition);
		}
	});
	
	var momHead = re.e('image mom-head.img mouse hit')
	.attr({ regX: 7, regY: 60 })
	.attr(re.getSVGElementPosition(svg, 'mom-head'))
	.on('click', function(x, y)
	{
		if (this.hit(x, y, 0, 0))
		{
			this.off('click');
			// Show thought bubble
			var thoughtBubble = re.e('image thought-bubble2.img tween')
			.attr(re.getSVGElementPosition(svg, 'thought-bubble'))
			.attr({ regX: 157, regY: 143, scaleX: 0, scaleY: 0 })
			.tween(1, { scaleX: 1, scaleY: 1 });
			
			setTimeout(function()
			{
				var heartDesaturated = re.e('image heart-desaturated.img')
				.attr(re.getSVGElementPosition(svg, 'heart'))
				.attr({ regX: 32, regY: 64 });
				
				var heart = re.e('image heart.img')
				.attr(re.getSVGElementPosition(svg, 'heart'))
				.attr({ regX: 32, regY: 64, sizeY: 0 });
				
				var updateHeart = function()
				{
					heart.attr({ sizeY: (love / 5) * 64 });
				};
			
				var minus = re.e('image minus.img mouse hit')
				.attr(re.getSVGElementPosition(svg, 'minus'))
				.on('click', function(x, y)
				{
					if (this.hit(x, y, 0, 0))
					{
						re.c('button.sfx').sound.play();
						if (love > 0)
							love--;
						updateHeart();
					}
				});
				
				var plus = re.e('image  plus.img mouse hit')
				.attr(re.getSVGElementPosition(svg, 'plus'))
				.on('click', function(x, y)
				{
					if (this.hit(x, y, 0, 0) && love < 5)
					{
						re.c('button.sfx').sound.play();
						love++;
						updateHeart();
						if (love == 5)
						{
							re.c('puzzle-solve.sfx').sound.play();
							minus.dispose();
							plus.dispose();
							setTimeout(function()
							{
								// Get rid of the thought bubble
								heartDesaturated.dispose();
								heart.dispose();
								thoughtBubble.tween(0.5, { scaleX: 0, scaleY: 0 })
								.on('tween:finish', function()
								{
									// Mom sits up
									momHead.dispose();
									this.dispose();
									momSittingUp = true;
									momCouch.flicker(2, [1, 2, 3]);
								});
							}, 2000);
						}
					}
				});
			}, 1000);
		}
	});
	
	var momCouch = re.e('sprite mom-couch.img flicker')
	.attr({ sizeX: 500, sizeY: 400 })
	.attr(re.getSVGElementPosition(svg, 'mom-couch'));
	
	var girls = re.e('image girls.img')
	.attr(re.getSVGElementPosition(svg, 'girls'));
	
	kidComputer = re.e('image kid-computer.img')
	.attr(re.getSVGElementPosition(svg, 'kid-computer'));
	
	monitor = re.e('image monitor.img')
	.attr(re.getSVGElementPosition(svg, 'monitor'));
	
	re('#fade')[0].drawLast();
})
.exit(function(nextScene)
{
	re('draw !overlay').dispose();
});