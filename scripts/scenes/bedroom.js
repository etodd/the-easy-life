re.scene('bedroom')
.enter(function(svg)
{
	re.log('Entered bedroom scene.');
	
	re('#fade')[0].tween(1.0, { opacity: 0 }); // Fade in
	
	re.e('image bedroom.img align');
	
	setTimeout(function()
	{
		var speechBubble = re.e('image speech-bubble.img tween')
		.attr(re.getSVGElementPosition(svg, 'dad-speech'))
		.attr(
		{
			regX: 60,
			regY: 150,
			scaleX: 0,
			scaleY: 0,
		});
		speechBubble.tween(0.5, { scaleX: 1, scaleY: 1 });
	}, 1500);
	
	setTimeout(function()
	{
		var speechBubble2 = re.e('image speech-bubble2.img tween')
		.attr(re.getSVGElementPosition(svg, 'mom-speech'))
		.attr(
		{
			regX: 0,
			regY: 33,
			scaleX: 0,
			scaleY: 0,
		});
		speechBubble2.tween(0.5, { scaleX: 1, scaleY: 1 });
	}, 3500);
	
	var drawerOpen = false, pageShown = false;
	
	re.e('sprite drawer.img mouse hit')
	.attr(
	{
		sizeX: 150,
		sizeY: 70,
	})
	.attr(re.getSVGElementPosition(svg, 'drawer'))
	.on('click', function(x, y)
	{
		if (this.hit(x, y, 0, 0))
		{
			if (!drawerOpen)
			{
				re.c('drawer.sfx').sound.play();
				this.frame(1);
				drawerOpen = true;
			}
			else if (!pageShown)
			{
				re.c('paper.sfx').sound.play();
				
				var actBubble = re.e('image act-bubble.img tween')
				.attr(re.getSVGElementPosition(svg, 'act-bubble'))
				.attr(
				{
					regX: 421,
					regY: 421,
					scaleX: 0,
					scaleY: 0,
				});
				actBubble.tween(0.5, { scaleX: 1, scaleY: 1 })
				.on('tween:finish', function()
				{
					var score = 21;
					
					var scoreText = re.e('text')
					.attr(
					{
						text: score.toString(),
						textColor: '#000',
						font: '24px sans-serif'
					})
					.attr(re.getSVGElementPosition(svg, 'score'));
					
					var updateScore = function()
					{
						scoreText.attr({ text: score.toString() });
					};
					
					var minus = re.e('image minus.img mouse hit')
					.attr(re.getSVGElementPosition(svg, 'minus'))
					.attr(
					{
						scaleX: 0.75,
						scaleY: 0.75,
					})
					.on('click', function(x, y)
					{
						if (this.hit(x, y, 0, 0))
						{
							if (score > 0)
								score--;
							re.c('button.sfx').sound.play();
							updateScore();
						}
					});
					
					var plus = re.e('image plus.img mouse hit')
					.attr(re.getSVGElementPosition(svg, 'plus'))
					.attr(
					{
						scaleX: 0.75,
						scaleY: 0.75,
					})
					.on('click', function(x, y)
					{
						if (this.hit(x, y, 0, 0))
						{
							score++;
							re.c('button.sfx').sound.play();
							updateScore();
							if (score == 34)
							{
								// Max score
								this.dispose();
								minus.dispose();
								re.c('puzzle-solve.sfx').sound.play();
								
								setTimeout(function()
								{
									re('#fade')[0].drawLast();
									re('#fade')[0].tween(1, {opacity: 1}); // Fade out
								}, 2000);
								
								setTimeout(function()
								{
									re.scene('load').enter('fin'); // Done fading out, load the next scene
								}, 4000);
							}
						}
					});
				});
		
				pageShown = true;
			}
		}
	});
	
	re('#fade')[0].drawLast();
})
.exit(function(nextScene)
{
	re('draw !overlay').dispose();
});