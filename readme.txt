The Easy Life
Evan Todd 2012

DEVELOPMENT
===========

You'll need to install EntityJS.
http://entityjs.com

Then just open a shell, navigate to the source directory and run this command:

entityjs server

Navigate to localhost:2345 to play the game.

ATTRIBUTION
===========

Music

"Sparkle Blue" by Jack Menhorn
http://soundcloud.com/jackmenhorn/sparkleblue-03

Sounds

"Ambi08.wav" by Ty Ford
http://www.freesound.org/people/Ty%20Ford/sounds/116660/

"BUBBLES POPPING.wav" by Ch0cchi
http://www.freesound.org/people/Ch0cchi/sounds/15349/

"bush11.wav" by schademans
http://www.freesound.org/people/schademans/sounds/2590/

"buttonchime02up.wav" by JustinBW
http://www.freesound.org/people/JustinBW/sounds/80921/

"click 1 d.wav" by TicTacShutUp
http://www.freesound.org/people/TicTacShutUp/sounds/406/

"RBH_Household_drawer 02.wav" by RHumphries
http://www.freesound.org/people/RHumphries/sounds/1009/

"camera click.mp3" by LS
http://www.freesound.org/people/LS/sounds/13658/

"btn121.wav" by junggle
http://www.freesound.org/people/junggle/sounds/29301/

"Page_Turn_29.wav" by Koops
http://www.freesound.org/people/Koops/sounds/20263/

"kung fu punch.aif" by nextmaking
http://www.freesound.org/people/nextmaking/sounds/86019/

"NZP BMW 1150GS Fuel pump.wav" by Noizemaker
http://www.freesound.org/people/Noizemaker/sounds/23216/